var ws;

document.addEventListener("DOMContentLoaded", function(event) {
    var cssId = 'support-css';  // you could encode the css path itself to generate id..
    if (!document.getElementById(cssId))
    {
        var head  = document.getElementsByTagName('head')[0];
        var link  = document.createElement('link');
        link.id   = cssId;
        link.rel  = 'stylesheet';
        link.type = 'text/css';
        link.href = 'http://192.168.1.180:8000/static/css/support.css';
        link.media = 'all';
        head.appendChild(link);
    }
    create_window();
    open_websocket();
});

function create_window(){
    var chat = document.createElement('div');
    chat.id="chat";
    chat.maxLength = "5000";
    var minimizer_wrapper = document.createElement('div');
    minimizer_wrapper.id = "minimizer_wrapper";
    var minimizer = document.createElement('div');
    minimizer.id = "minimizer";
    var minimizer_inner = document.createElement('div');
    var feed = document.createElement('div');
    feed.id="feed";
    var input = document.createElement("textarea");
    input.name = "textarea";
    input.id = "textarea";
    input.maxLength = "5000";
    input.cols = "30";
    input.rows = "5";
    document.body.appendChild(chat);
    chat.appendChild(minimizer_wrapper);
    chat.appendChild(feed);
    chat.appendChild(input);
    minimizer_wrapper.appendChild(minimizer);
    minimizer.appendChild(minimizer_inner);
    var minimized = document.createElement('div');
    minimized.className = 'masked';
    minimized.id="minimized";
    minimized.setAttribute('onclick', 'maximize()');
    minimizer.setAttribute('onclick', 'minimize()');
    document.body.appendChild(minimized);
}

function open_websocket(){
    function do_command(command) {
        if (command == "clear") {
            var feed = document.getElementById('feed');
            feed.innerHTML = ""
        }
    }

    function show_message(data){
        var author = data[0];
        var message = data[1];
        var el = document.createElement('div');
        el.innerHTML = message;
        el.className +=' message';
        el.className +=' ' + author;
        var feed = document.getElementById('feed');
        feed.appendChild(el);
        feed.scrollTop = feed.scrollHeight;
    }
    var room = getCookie("room");
    if (!room) {
        room = Date.now();
        setCookie("room", room);
    }
    ws = new WebSocket("ws://192.168.1.180:8888/room/" + room);
    ws.onmessage = function(event) {
        data = JSON.parse(event.data);
        if (data[0] == "command") {
            do_command(data[1]);
        }
        else {
            show_message(data, false);
        }
    };
    document.getElementById('textarea').onkeydown = function(e) {
        if (e.keyCode == 13) {
            send_message();
            return false;
        }
    };
}

function send_message() {
    var textarea = document.getElementById('textarea');
    if (textarea.value == "") {
        return false;
    }
    if (ws.readyState != WebSocket.OPEN) {
        return false;
    }
    ws.send(textarea.value);
    textarea.value = "";
}

function setCookie(name, value, options) {
  options = options || {};

  var expires = options.expires;

  if (typeof expires == "number" && expires) {
    var d = new Date();
    d.setTime(d.getTime() + expires * 1000);
    expires = options.expires = d;
  }
  if (expires && expires.toUTCString) {
    options.expires = expires.toUTCString();
  }

  value = encodeURIComponent(value);

  var updatedCookie = name + "=" + value;

  for (var propName in options) {
    updatedCookie += "; " + propName;
    var propValue = options[propName];
    if (propValue !== true) {
      updatedCookie += "=" + propValue;
    }
  }

  document.cookie = updatedCookie;
}

function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}



function maximize() {
    document.getElementById('minimized').className = "masked";
    document.getElementById('chat').className = "";
    document.getElementById('textarea').focus();
};

function minimize() {
    document.getElementById('chat').className = "masked";
    document.getElementById('minimized').className = "";
};